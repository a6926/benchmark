# benchmark

Bench scripts

## Postgres

Run postgres instance

```sh
docker run --ulimit memlock=-1:-1 -it --rm=true --memory-swappiness=0 \
    --name twitter-clone -e POSTGRES_USER=twitter \
    -e POSTGRES_PASSWORD=twitter -e POSTGRES_DB=twitter \
    -p 5432:5432 postgres:latest
```

## Bombardier benchmark

Install bombardier : https://github.com/codesenberg/bombardier

```sh
go get -u github.com/codesenberg/bombardier
```

Bench endpoint for 10 seconds using 200 connections.

```sh
./bombardier -c 200 -d 10s -l http://localhost:8082/tweets
```